#pragma once

#include <iostream>
#include <list>
#include <vector>
using namespace std;

#ifndef GRAPH_H
#define GRAPH_H

class Node;

class Edge
{
public:
	Edge(Node* start, Node* end, int num, char edgeName)
	{
		head = start;
		tail = end;
		weight = num;
		name = edgeName;
	}

	Node* head;
	Node* tail;
	int weight;
	char name;
};

class Node
{
public:

	void addEdge(Node* end, int weight, char edgeName) //add edge between node
	{
		Edge tmp(this, end, weight, edgeName); //create edge
		edge.push_back(tmp); //push edge to list 
	}

	void printEdge() //print edge
	{
		cout << "Node: " << name << endl;
		if (edge.empty())
		{
			cout << " No Destination.." << endl;
		}
		for (auto it = edge.begin(); it != edge.end(); it++)
		{
			Edge  line = *it;
			cout << " -> " << line.tail->name << ", weight : " << line.weight << endl;
		}
	}

	char name;
	list<Edge> edge;
};

class Graph
{
public:

	void showGraph() //print graph
	{
		for (auto it = nodes.begin(); it != nodes.end(); it++)
		{
			Node nodes = *it;
			nodes.printEdge(); //print edge of each node
		}
	}

	void graphForm(int arr[4][4], int node_size) //change adjacency matrix[array] to list
	{
		
		Node node_name;

		for (int i = 0; i < node_size; i++) //create node of graph
		{
			node_name.name = (65 + i); //set name 
			nodes.push_back(node_name); //push the name of node
		}

		int row = 0;
		for (auto it = nodes.begin(); it != nodes.end(); it++) //create edge of graph
		{
			for (int column = 0; column < node_size; column++)
			{
				Node* end = new Node;
				Node& start = *it;
				if (arr[row][column] == 0)
				{
					continue;
				}
				else {
					end->name = 65 + column;
					char edgeName = 65 + column;
					start.addEdge(end, arr[row][column], edgeName);
				}
			}
			row++; //update row
		}
	}

	bool weightGraph()
	{
		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse to every node
		{
			Node nodes = *it;
			list<Edge> node_edge = nodes.edge; //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				if (it2->weight != 0)
				{
					return true;
				}
			}
		}
		return false;
	}

	bool completedGraph()
	{
		int count = 0;
		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse through nodes
		{
			Node nodes = *it;
			list<Edge> node_edge = nodes.edge; //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				Node* start = it2->head;
				Node* end = it2->tail;
				count++;
				if (start->name== end->name)
				{
					count--;
				}
			}
		}
		if (count == nodes.size() * (nodes.size() - 1) / 2)
		{
			return true;
		}
		return false;
	}

	bool pseudoGraph()
	{
		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse through nodes
		{
			Node nodes = *it;
			list<Edge> node_edge = nodes.edge; //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				Node* start = it2->head;
				Node* end = it2->tail;
				if (start->name == end->name && it2->weight != 0)
				{
					return true;
				}
			}
		}
		return false;
	}

	bool digraph()
	{
		for (auto it = nodes.begin(); it != nodes.end(); it++) //go through noded
		{
			Node node = *it;
			list<Edge> node_edge = node.edge; //get edge between each nodes

			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				Node* start = it2->head;
				Node* end = it2->tail;

				for (auto it3 = nodes.begin(); it3 != nodes.end(); it3++)
				{
					Node node_2 = *it3;
					list<Edge> node_edge2 = node_2.edge; //get edge between each nodes

					if (node_2.name != node.name)
					{
						for (auto it4 = node_edge2.begin(); it4 != node_edge2.end(); it4++)
						{
							Node* start_2 = it4->head;
							Node* end_2 = it4->tail;

							if (start->name == end_2->name && start_2->name == end->name && (start->name != end->name) || start_2->name != end_2->name)
							{
								return false;
							}
						}
					}

				}
			}
		}
	}

	bool multiGraph()
	{
		if (pseudoGraph()) {
			return false;
		}

		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse to every node
		{
			vector<char>edgeName;
			Node nodes = *it;
			list<Edge> node_edge = nodes.edge; //get edge between each nodes
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				if (edgeName.size() > 0) {
					for (int i = 0; i < edgeName.size(); ++i) {
						for (int j = i + 1; j < edgeName.size(); ++j) {
							if (edgeName[i] == edgeName[j])
								return true;
						}
					}
				}

				edgeName.push_back(it2->name);
				
			}
		}

		return false;
		
	}
	
	
	
	void dykstra(char head) //shortest path
	{
		char name_start = head;
		vector<int> short_weight;
		vector<char> short_destination;
		vector<int> weight;
		vector<char> destination;

		short_weight.push_back(0); //the shortest weight is 0
		short_destination.push_back(head);
		weight.push_back(0);
		destination.push_back(head);

		for (auto it = nodes.begin(); it != nodes.end(); it++) //traverse node
		{
			Node node = *it;
			list<Edge> node_edge = node.edge; //get edge
			for (auto it2 = node_edge.begin(); it2 != node_edge.end(); it2++)
			{
				Node* strt = it2->head;
				Node* end = it2->tail;
				int distance = it2->weight;

				if (strt->name == head)
				{
					weight.push_back(distance);
					destination.push_back(end->name);
				}

				int find_min = weight[0];
				char min_name = destination[0];
				
				for (int i = 0; i < weight.size(); i++) //find minimum weight 
				{
					if (find_min > weight[i] && weight[i] > 0)
					{
						find_min = weight[i];
						min_name = destination[i];
					}
				}

				short_destination.push_back(min_name);
				short_weight.push_back(find_min);
				head = min_name;

				for (int i = 0; i < destination.size(); i++) //edge that point at itself, weight equal to 0
				{
					if (destination[i] == min_name)
					{
						weight[i] = 0;
					}
				}

				while (true)
				{
					for (auto it3 = nodes.begin(); it3 != nodes.end(); it3++)
					{
						Node node_2 = *it3;
						list<Edge> node_edge2 = node_2.edge; //get edge between each nodes

						for (auto it4 = node_edge.begin(); it4 != node_edge.end(); it4++)
						{
							Node* strt = it4->head;
							Node* end = it4->tail;
							int distance = it4->weight;
							bool status = false;

							if (strt->name == head)
							{

								for (int i = 0; i < short_destination.size(); i++)
								{
									if (short_destination[i] == end->name) //if already shortest
									{
										status = true;
										break;
									}
								}

								if (status == false)
								{
									for (int i = 0; i < destination.size(); i++)
									{
										if (destination[i] == end->name && weight[i] > find_min + distance)
										{
											weight[i] = find_min + distance;
											status = true;
											break;
										}
									}
									if (status == false)
									{
										int w = distance + find_min;  //shortest value from start
										weight.push_back(w);
										destination.push_back(end->name);
									}
								}
							}
						}
					}

					for (int i = 0; i < weight.size(); i++)
					{
						if (weight[i] > 0)
						{
							min_name = destination[i];
							find_min = weight[i];
							break;
						}
					}

					for (int i = 0; i < weight.size(); i++)
					{
						if (find_min > weight[i] && weight[i] > 0)
						{
							min_name = destination[i];
							find_min = weight[i];
						}
					}

					if (min_name != short_destination.back())
					{
						bool havenew = true;
						int index;

						for (int i = 0; i < short_destination.size(); i++) { //is the value is a new value
							if (short_destination[i] == min_name) {
								havenew = false;
								index = i;
							}
						}

						if (havenew == true) { //if yes add new value
							short_destination.push_back(min_name);
							short_weight.push_back(find_min);
						}
						else if (short_weight[index] > find_min) { //if not change the old value
							short_destination[index] = min_name;
							short_weight[index] = find_min;
						}

					}

					for (int i = 0; i < destination.size(); i++)
					{
						if (destination[i] == min_name)
						{
							weight[i] = 0;
						}
					}

					int count = 0;
					head = min_name;
					for (int i = 0; i < weight.size(); i++)
					{
						if (weight[i] == 0)
						{
							count++;
						}
					}
					if (count == weight.size())
					{
						break;
					}
				}
			}
		}


		//show shortest distance
		for (int i = 0; i != short_destination.size(); i++)
		{
			if(name_start != short_destination[i] && short_weight[i] > 0)
				cout << name_start << " to " << short_destination[i] << " with distance " << short_weight[i] << endl;
		}
	}

	list<Node> nodes;
};

#endif